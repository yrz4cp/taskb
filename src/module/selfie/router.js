const Router = require("express").Router();
const multer = require("multer");

const controllers = require("./controllers");

const storageMemory = multer.memoryStorage({
  destination: (req, files, callback) => {
    callback(null, "");
  },
});

const multipleUpload = multer({ storage: storageMemory }).array("files");

Router.post.apply(Router, [
  "/",
  // authentication,
  multipleUpload,
  // ...validations.create,
  asyncRouteWrapper(controllers.create),
]);

module.exports = Router;
