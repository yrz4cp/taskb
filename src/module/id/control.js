const awsService = require("../../services/aws_s3_service");

module.exports.create = async (req, res, next) => {
  const { files: uploads } = req;
  console.log(uploads);

  const response = [];
  for (let i = 0; i < uploads.length; i++) {
    const file = uploads[i];
    const result = await awsService.upload(file);
    // const row = await files.create({ name: file.originalname, path: result });
    response.push({ path: result });
  }

  res.success.Created("file.created", response);
};
