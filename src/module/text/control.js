const jwt = require("jsonwebtoken");

module.exports.text = async (req, res) => {
  const result = await this.text.create({
    register: req.body.register,
  });

  const data = {
    id: result.id,
    register: result.register,
  };

  const rtoken = jwt.sign(data, process.env.RTOKEN);
  res.success.text("worked", rtoken);
};
