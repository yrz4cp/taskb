const aws = require("aws-sdk");

const ID = process.env.S3_BUCKET_ID;
const SECRET = process.env.S3_BUCKET_SECRET;
const BUCKET_NAME = process.env.S3_BUCKET_NAME;

aws.config.update({
  accessKeyId: ID,
  secretAccessKey: SECRET,
  region: "ap-southeast-1",
});

const s3bucket = new aws.S3();

module.exports.upload = async (file) => {
  const params = {
    ACL: "public-read",
    ContentType: file.contentType,
    Bucket: BUCKET_NAME,
    Key: file.originalname, // file name you want to save as
    Body: file.buffer,
  };

  return s3bucket
    .upload(params)
    .promise()
    .then((data) => {
      return data.Location;
    })
    .catch((err) => {
      throw err;
    });
};

module.exports.remove = async (name) => {
  const params = {
    Bucket: BUCKET_NAME,
    Delete: {
      Objects: [{ Key: name }],
    },
  };

  return s3bucket
    .deleteObjects(params)
    .promise()
    .then((data) => {
      return data.Location;
    })
    .catch((err) => {
      throw err;
    });
};
