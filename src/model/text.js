module.exports = (sequelize, DataTypes) => {
  const accounts = sequelize.define("accounts", {
    register_number: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  });

  return accounts;
};
