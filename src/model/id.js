module.exports = (sequelize, DataTypes) => {
  const files = sequelize.define(
    "files",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      path: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      freezeTableName: true,
    }
  );

  return files;
};
